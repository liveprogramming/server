package com.liveprogramming.kingslife.server.model;

import com.jona446.jhudp.JUSocket;
import com.jona446.jhudp.Message;
import com.liveprogramming.kingslife.server.ServerManager;
import com.liveprogramming.kingslife.server.networkcommunication.Connection;
import com.liveprogramming.kingslife.server.networkcommunication.UDPHandler;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

import java.io.IOException;
import java.net.InetAddress;
import java.time.LocalDateTime;

public class Client {

  private Connection tcp;
  private UDPHandler udp;
  private JUSocket jhudp;

  public InetAddress address;
  public int udpPort = -1;

  public final long id;
  public SimpleStringProperty playername = new SimpleStringProperty("");
  public int nameUpdateSequence = Integer.MIN_VALUE;
  public boolean player = true;
  public Team team;

  public LocalDateTime connected = LocalDateTime.now();

  // In game stats
  public byte health = 100;

  public int sendSequence = Integer.MIN_VALUE, receiveSequence = Integer.MIN_VALUE;

  public enum Team {
    BLUE('0'), RED('1');

    public char character;

    Team(char c) {
      this.character = c;
    }

    public Team fromCharacter(char c) {
      for(Team t : Team.values()) {
        if(t.character == c) return t;
      }
      return null;
    }
  }

  public Client(Connection tcp, UDPHandler udp, long id, InetAddress address) {
    this.tcp = tcp;
    this.udp = udp;
    this.id = id;
    this.address = address;
  }

  public Client(JUSocket jhudp, long id, InetAddress address, int port) {
    this.jhudp = jhudp;
    this.id = id;
    this.address = address;
    this.udpPort = port;
  }

  public void send(String message, boolean reliable) {
    Message msg = new Message(message.getBytes(), this.address, this.udpPort, reliable);
    send(msg);
  }

  public void send(Message message) {
    if(jhudp != null) {
      try {
        if(jhudp == null) return;
        jhudp.send(message);
      } catch (IOException ex) {}
    } else if(tcp != null) {
      if(message.getReliable() || udpPort == -1) {
        if(tcp == null) return;
        tcp.send(message);
      } else {
        if(udp == null) return;
        udp.send(message);
      }
    }
  }

  @Override
  public String toString() {
    return (player ? "Player " : "Spectator ") + id + " (" + address.getHostAddress() + ")";
  }

}
