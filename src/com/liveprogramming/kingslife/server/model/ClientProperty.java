package com.liveprogramming.kingslife.server.model;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleStringProperty;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class ClientProperty {

  public final ReadOnlyStringWrapper key;
  public final SimpleStringProperty value;

  private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm:ss a");

  private ClientProperty(String key, SimpleStringProperty value) {
    this.key = new ReadOnlyStringWrapper(key);
    this.value = value;
  }

  public static ArrayList<ClientProperty> fromPlayer(Client client) {
    ArrayList<ClientProperty> props = new ArrayList<>(5);
    props.add(new ClientProperty("ID", new SimpleStringProperty(client.id + "")));
    props.add(new ClientProperty("IP", new SimpleStringProperty(client.address.getHostAddress())));
    props.add(new ClientProperty("Name", client.playername));
    props.add(new ClientProperty("Type", new SimpleStringProperty(client.player ? "Player" : "Spectator")));
    props.add(new ClientProperty("Connected", new SimpleStringProperty(formatter.format(client.connected))));
    return props;
  }

}
