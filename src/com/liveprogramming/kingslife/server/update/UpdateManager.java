package com.liveprogramming.kingslife.server.update;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Consumer;

public class UpdateManager {

  private static final UpdateManager instance = new UpdateManager();
  private UpdateManager() {}
  public static UpdateManager getInstance() {
    return instance;
  }

  public void setLogger(Consumer<String> logger) {
    this.logger = logger;
  }

  private Consumer<String> logger;
  private ServerSocket serverSocket;
  private boolean running;

  private File currentFile;
  private long currentVersion = -1;

  private ConcurrentLinkedQueue<UpdateConnection> connections = new ConcurrentLinkedQueue<>();

  public void start() {
    if(!running) {
      try {
        serverSocket = new ServerSocket(8886);

        VersionFile vf = new VersionFile();
        long version = vf.getVersion();
        if(version != -1) {
          currentVersion = version;
          currentFile = vf.getFile();
          if(!currentFile.exists()) {
            logger.accept("Last known game file not found!");
          } else {
            logger.accept("Game file found. Current version is " + currentVersion);
          }
        }

        openConnection();

        running = true;
        logger.accept("UpdateManager started!");
      } catch (Exception ex) {
        ex.printStackTrace();
        logger.accept("Couldn't start UpdateManager!");
      }
    } else {
      logger.accept("UpdateManager already running!");
    }
  }

  public void stop() {
    if(running) {
      try {
        connections.forEach(c -> c.close());
        serverSocket.close();

        running = false;
        logger.accept("UpdateManager stopped!");
      } catch (Exception ex) {
        logger.accept("Couldn't stop UpdateManager!");
      }
    } else {
      logger.accept("UpdateManager not running!");
    }
  }

  void openConnection() {
    UpdateConnection connection = new UpdateConnection(serverSocket);
    connections.add(connection);
    connection.start();
  }

  void removeConnection(UpdateConnection connection) {
    connections.remove(connection);
  }

  void update() {
    try {
      FileInputStream fis = new FileInputStream(currentFile);
      LinkedList<UpdateConnection> clients = new LinkedList<>();
      connections.forEach(connection -> {
        if(connection.isConnected() && connection.getVersion() < currentVersion) {
          clients.add(connection);
        }
      });
      clients.forEach(c -> {
        try {
          c.sendVersion(currentVersion);
        } catch (IOException ex) {
          ex.printStackTrace();
        }
      });

      byte[] buffer = new byte[4096];
      int bytesRead;
      while((bytesRead = fis.read(buffer)) != -1) {
        for(UpdateConnection connection : clients) {
          connection.send(buffer, bytesRead);
        }
      }
      clients.forEach(c -> {
        try {
          c.flush();
          c.close();
        } catch (IOException ex) {
          ex.printStackTrace();
        }
      });
      fis.close();
      logger.accept("Deployed game to " + clients.size() + " client(s)");
    } catch (Exception ex) {
      ex.printStackTrace();
      logger.accept("Couldn't deploy to clients!");
    }
  }

  public void deploy(File file) {
    currentVersion++;
    currentFile = file;
    try {
      new VersionFile().update(currentFile, currentVersion);
    } catch (IOException ex) {
      ex.printStackTrace();
    }
    update();
  }

  public long getCurrentVersion() {
    return currentVersion;
  }

}
