package com.liveprogramming.kingslife.server.update;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class VersionFile {

  private File file = new File("version.properties");

  public void update(File f, long version) throws IOException {
    if(!file.exists()) file.createNewFile();

    PrintWriter writer = new PrintWriter(file);
    writer.println(version + "," + f.getAbsolutePath());
    writer.flush();
    writer.close();
  }

  public long getVersion() throws FileNotFoundException {
    if(file.exists()) {
      Scanner scanner = new Scanner(file);
      if(scanner.hasNext()) {
        String line = scanner.nextLine();
        String[] parts = line.split(",");
        scanner.close();
        return Long.parseLong(parts[0]);
      }
    }
    return -1;
  }

  public File getFile() throws FileNotFoundException {
    if(file.exists()) {
      Scanner scanner = new Scanner(file);
      if(scanner.hasNext()) {
        String line = scanner.nextLine();
        String[] parts = line.split(",");
        scanner.close();
        return new File(parts[1]);
      }
    }
    return null;
  }

}
