package com.liveprogramming.kingslife.server.update;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class UpdateConnection extends Thread {

  private ServerSocket serverSocket;
  private Socket socket;
  private OutputStream output;
  private boolean connected;

  private long version = -1;

  public UpdateConnection(ServerSocket serverSocket) {
    this.serverSocket = serverSocket;
  }

  @Override
  public void run() {
    try {
      socket = serverSocket.accept();
      socket.setKeepAlive(true);
      socket.setSoTimeout(0);
      connected = true;

      UpdateManager.getInstance().openConnection();

      BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      output = socket.getOutputStream();

      String line;
      while((line = reader.readLine()) != null) {
        version = Long.parseLong(line);
        if(version < UpdateManager.getInstance().getCurrentVersion()) {
          UpdateManager.getInstance().update();
        }
      }
      socket.close();
    } catch (SocketException ex) {
      // TODO: Handle?
    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      UpdateManager.getInstance().removeConnection(this);
    }
  }

  void sendVersion(long version) throws IOException {
    String send = version + "\n";
    output.write(send.getBytes());
    output.flush();
  }

  void send(byte[] buffer, int bytes) throws IOException {
    output.write(buffer, 0, bytes);
  }

  void flush() throws IOException {
    output.flush();
  }

  public long getVersion() {
    return version;
  }

  public boolean isConnected() {
    return connected;
  }

  public void close() {
    try {
      interrupt();
      socket.close();
    } catch (Exception ex) {}
  }

}
