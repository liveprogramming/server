package com.liveprogramming.kingslife.server.networkcommunication;

import com.jona446.jhudp.Message;
import com.jona446.jhudp.MessageReceiver;
import com.liveprogramming.kingslife.server.ServerManager;
import com.liveprogramming.kingslife.server.model.Client;
import com.liveprogramming.kingslife.server.model.Game;

import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

public class MessageHandler implements MessageReceiver {

  private ServerManager serverManager = ServerManager.getInstance();
  public boolean inGame = false;

  private Game game;

  @Override
  public void receive(Message message) {
    String raw = message.getDataAsString();
    String msg = raw.substring(1);
    msg = msg.replace("\n", "");

    char type = raw.charAt(0);
    switch(type) {
      case 'C': { // Client connects (pre-game menu) [C]
        if(!serverManager.onClientConnected(message.getAddress(), message.getPort())) {
          return;
        }
        Client client = serverManager.getClient(message.getAddress());
        client.player = msg.equalsIgnoreCase("0");

        // Send current game status to new client [S]
        client.send(("S" + (inGame ? "1" : "0")), true);

        // Synchronize time [M]
        client.send("M" + System.currentTimeMillis(), true);

        LinkedList<Client> clients = serverManager.getClients();

        // Send already connected players to new client [J]
        StringBuilder connectedPlayers = new StringBuilder("J");
        clients.forEach(p -> {
          if(p.player && p.id != client.id) {
            connectedPlayers.append(p.id);
            connectedPlayers.append(",");
          }
        });
        connectedPlayers.setLength(connectedPlayers.length() - 1);
        client.send(connectedPlayers.toString(), true);

        // Send playernames for connected players to new client [U]
        clients.forEach(p -> {
          if(p.player) {
            client.send("U" + p.id + "," + p.playername.get() + "," + p.nameUpdateSequence, true);
          }
        });

        // Choose team for player
        short blue = 0, red = 0;
        for(Client c : clients) {
          if(c.player) {
            if(c.team == Client.Team.BLUE) blue++;
            else red++;
          }
        }
        if(red < blue) client.team = Client.Team.RED;
        else client.team = Client.Team.BLUE;

        clients.forEach(p -> {
          if(p.player) {
            // Send teams to newly connected client [Y]
            client.send("Y" + p.id + "," + p.team.character, true);
            // Send health to newly connected client [H]
            client.send("H" + p.id + "," + p.health, true);
          }
        });

        if(client.player) {
          // Alert other clients to newly connected player [J]
          serverManager.broadcast("J" + client.id, true, client);
          // Send newly connected player's team to other clients
          serverManager.broadcast("Y" + client.id + "," + client.team.character, true, client);
        }

        // Send updates regarding map to newly connected client
        if(game != null) {
          final Message[] messages = game.mapMessages.toArray(new Message[0]);
          new Thread(() -> {
            for(Message m : messages) {
              client.send(new Message(m.getData(), m.getReliable()));
            }
          }).start();
        }
        break;
      }
      case 'D': { // Client disconnects [D]
        Client client = serverManager.getClient(message.getAddress());

        // Confirm disconnect [D]
        client.send("D", true);

        serverManager.onClientDisconnected(client);
        break;
      }
      case 'T': { // Start game [T]
        inGame = true;
        game = new Game();

        // Notify other clients of game start [T]
        serverManager.broadcast("T", true, null);
        break;
      }
      case 'U': { // Update playername [U]
        Client client = serverManager.getClient(message.getAddress());
        if(client != null) {
          String parts[] = msg.split(",");
          int nameUpdateSequence = Integer.parseInt(parts[1]);
          if(nameUpdateSequence > client.nameUpdateSequence) {
            client.nameUpdateSequence = nameUpdateSequence;
            client.playername.set(parts[0]);

            if(client.player) {
              // Notify players of updated playername [U]
              serverManager.broadcast("U" + client.id + "," + client.playername.get() + "," + client.nameUpdateSequence, true, null);
            }
          }
        }
        break;
      }
      case 'Y': { // Set team [Y]
        Client client = serverManager.getClient(message.getAddress());
        if(client != null) {
          client.team = Client.Team.RED.fromCharacter(msg.charAt(0));

          // Send team update to clients
          serverManager.broadcast("Y" + client.id + "," + client.team.character, true, null);
        }
        break;
      }
      case 'X': { // Update player location [X]
        Client client = serverManager.getClient(message.getAddress());

        // Send new player location to clients [X]
        if(client != null) {
          int sequence = Integer.parseInt(msg.substring(msg.lastIndexOf(",") + 1));
          String mes = "X" + client.id + "," + msg.substring(0, msg.lastIndexOf(","));
          if(sequence > client.receiveSequence ||
                  (sequence < client.receiveSequence && Math.abs(sequence - client.receiveSequence) >= Integer.MAX_VALUE)) {
            client.receiveSequence = sequence;
            serverManager.getClients().forEach(c -> {
              if(c != client) {
                c.send( mes + "," + c.sendSequence, true);
                if(c.sendSequence < Integer.MAX_VALUE) {
                  c.sendSequence++;
                } else {
                  c.sendSequence = Integer.MIN_VALUE;
                }
              }
            });
          }
        }
        break;
      }
      case 'V': { // Shot fired by client [V]
        Client client = serverManager.getClient(message.getAddress());

        if(client != null) {
          // Send shot to other clients [V]
          serverManager.broadcast("V" + client.id + "," + msg, false, client);
        }
        break;
      }
      case 'I': { // Item used [I]
        Client client = serverManager.getClient(message.getAddress());

        // Send item usage to other clients [I]
        serverManager.broadcast("I" + client.id + "," + msg, true, null);
        game.mapMessages.add(message);
        break;
      }
      case 'R': { // Object damaged [R]
        Client client = serverManager.getClient(message.getAddress());

        // Send damage to clients [R]
        serverManager.broadcast("R" + client.id + "," + msg, true, null);
        game.mapMessages.add(message);
        break;
      }
      case 'H': { // Health updated [H]
        Client client = serverManager.getClient(message.getAddress());
        client.health += Short.parseShort(msg);

        // Send new health to clients [H]
        serverManager.broadcast("H" + client.id + "," + client.health, true, null);

        if(client.health <= 0) {
          // Send respawn time to clients [N]
          long respawnTime = 10 + game.deaths;
          serverManager.broadcast("N" + client.id + "," + respawnTime, true, null);
          new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
              if(client != null) {
                client.health = 100;
                game.deaths++; // TODO: Decide -> does this make respawn timers more interesting?
              }
            }
          }, respawnTime * 1_000 - 10);
        }
        break;
      }
      case 'F': { // Game ends [F]
        // Notify other clients of game completion [F]
        serverManager.broadcast("F" + msg, true, null);
        inGame = false;
        game = new Game();
        break;
      }
      default: {
        System.out.println("Unknown message type: " + type);
        break;
      }
    }
  }

  public void receivedUDPMessage(Message message) {
    Client client = serverManager.getClient(message.getAddress());
    if(client != null) client.udpPort = message.getPort();
  }

}
