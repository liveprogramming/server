package com.liveprogramming.kingslife.server.networkcommunication;

import com.jona446.jhudp.Message;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Socket;
import java.net.SocketException;

public class UDPHandler extends Thread{

  public UDPHandler(MessageHandler messageHandler, int packet_size) {
    this.messageHandler = messageHandler;
    this.packet_size = packet_size;
  }

  private MessageHandler messageHandler;
  private int packet_size;
  private DatagramSocket socket;

  private byte[] receiveBuffer;
  private DatagramPacket receivePacket;

  @Override
  public void run() {
    try {
      socket = new DatagramSocket(8885);
      receiveBuffer = new byte[packet_size];
      receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
      while(!interrupted()) {
        try {
          socket.receive(receivePacket);
          Message message = new Message(receivePacket.getData(), receivePacket.getAddress(), receivePacket.getPort(), false);
          messageHandler.receivedUDPMessage(message);
          messageHandler.receive(message);
        } catch (SocketException ex) {
          // TODO: Handle?
        } catch (Exception ex) {
          ex.printStackTrace();
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void send(Message message) {
    try {
      byte[] data = message.getData();
      if(data.length < packet_size) {
        byte[] temp = new byte[packet_size];
        System.arraycopy(data, 0, temp, 0, data.length);
        data = temp;
      }
      socket.send(new DatagramPacket(data, packet_size));
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  public void close() {
    interrupt();
    socket.close();
  }

}
