package com.liveprogramming.kingslife.server.networkcommunication;

import com.jona446.jhudp.Config;
import com.jona446.jhudp.Message;
import com.liveprogramming.kingslife.server.ServerManager;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class Connection extends Thread{

  private ServerSocket serverSocket;
  private volatile Socket socket;

  private MessageHandler messageHandler;
  private volatile BufferedReader reader;
  private volatile PrintWriter writer;
  private Config config;

  private InetAddress address;

  public Connection(ServerSocket serverSocket, MessageHandler messageHandler, Config config) {
    this.serverSocket = serverSocket;
    this.config = config;
    this.messageHandler = messageHandler;
  }

  public void run() {
    try {
      socket = serverSocket.accept();
      socket.setTcpNoDelay(true);
      socket.setKeepAlive(true);

      address = socket.getInetAddress();
      ServerManager.getInstance().openConnection();

      reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      writer = new PrintWriter(socket.getOutputStream());

      char[] buffer = new char[config.packet_size];
      while(!interrupted() && (reader.read(buffer)) != -1) {
        Message message = new Message(new String(buffer).getBytes(), socket.getInetAddress(), socket.getPort());
        received(message);
      }

      close();
    } catch (SocketException ex) {
      // TODO: Handling?
    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      if(address != null) config.disconnectHandler.accept(address, -1);
    }
  }

  private void received(Message message) {
    messageHandler.receive(message);
  }

  public void send(Message message) {
    byte[] data = message.getData();
    if(data.length < config.packet_size) {
      byte[] temp = new byte[config.packet_size];
      System.arraycopy(data, 0, temp, 0, data.length);
      data = temp;
    }
    writer.write(new String(data).toCharArray());
    writer.flush();
  }

  public Socket getSocket() {
    return socket;
  }

  public void close() {
    try {
      interrupt();
      if(socket != null) {
        socket.close();
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

}
