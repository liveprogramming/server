package com.liveprogramming.kingslife.server;

import com.jona446.jhudp.Config;
import com.jona446.jhudp.JUSocket;
import com.jona446.jhudp.Message;
import com.jona446.jhudp.MessageReceiver;
import com.liveprogramming.kingslife.server.model.Client;
import com.liveprogramming.kingslife.server.networkcommunication.Connection;
import com.liveprogramming.kingslife.server.networkcommunication.MessageHandler;
import com.liveprogramming.kingslife.server.networkcommunication.UDPHandler;
import javafx.application.Platform;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;

public class ServerManager {

  public static ServerManager instance = new ServerManager();
  private ServerManager() {}

  public static ServerManager getInstance() {
    return instance;
  }

  private Consumer<String> logger;

  private boolean running = false;

  private UDPHandler udpHandler;
  private ServerSocket serverSocket;

  private ConcurrentHashMap<InetAddress, Client> clients = new ConcurrentHashMap<>();
  private ObservableList<Client> clientsObs;
  private int id = 0;

  private Method method;
  private CopyOnWriteArrayList<Connection> connections = new CopyOnWriteArrayList<>();

  private JUSocket socket;
  private MessageHandler receiver;
  private Config config;

  public enum Method {
    TCPUDP, JHUDP;
  }

  public void setLogger(Consumer<String> logger) {
    this.logger = logger;
  }

  public void setPlayers(ObservableList<Client> clients) {
    this.clientsObs = clients;
  }

  public void start(Method method) {
    if(!running) {
      this.method = method;
      try {
        config = new Config();
        config.packet_size = 50;
        config.keepAlive = true;
        receiver = new MessageHandler();
        config.receiver = receiver;
        config.disconnectHandler = this::onClientDisconnected;
        switch(method) {
          case JHUDP: {
            socket = new JUSocket(config, 8884);
            break;
          }
          case TCPUDP: {
            // TCP
            serverSocket = new ServerSocket(8884);

            openConnection();

            // UDP
            udpHandler = new UDPHandler((MessageHandler) receiver, config.packet_size);
            udpHandler.start();
            break;
          }
        }

        logger.accept("Started server with " + method.name());
        running = true;
      } catch (Exception ex) {
        logger.accept("Couldn't start server");
        ex.printStackTrace();
      }
    } else {
      logger.accept("Server already running");
    }
  }

  public void stop() {
    if(running) {
      broadcast("D", true, null);
      clearClients();
      try {
        Thread.sleep(50);
      } catch (InterruptedException e) {}

      switch(method) {
        case JHUDP: {
          socket.close();
          break;
        }
        case TCPUDP: {
          // TCP
          for(Connection connection : connections) {
            closeConnection(connection);
          }
          try {
            serverSocket.close();
          } catch (IOException ex) {
            ex.printStackTrace();
          }
          id = 0;

          // UDP
          udpHandler.close();
          break;
        }
      }

      running = false;
      logger.accept("Stopped server");
    } else {
      logger.accept("Server not running");
    }
  }

  public boolean onClientConnected(InetAddress address, int port) {
    if(getClient(address) != null) return false;

    Client client;
    if(method == Method.JHUDP) {
      client = new Client(socket, id++, address, port);
    } else {
      client = new Client(getConnection(address), udpHandler, id++, address);
    }
    addClient(client);
    client.send(new Message(("C" + client.id).getBytes(), client.address, client.udpPort, true));
    logger.accept("Client " + client.id + " connected");
    return true;
  }

  public void onClientDisconnected(InetAddress address, int port) {
    onClientDisconnected(getClient(address));
  }

  public void onClientDisconnected(Client client) {
    if(client != null) {
      if(client.player) {
        // Notify other clients of disconnected player [K]
        broadcast("K" + client.id, true,  client);
      }
      logger.accept("Client " + client.id + " disconnected");
      removeClient(client);
    }
  }

  private void addClient(Client client) {
    Platform.runLater(() -> clientsObs.add(client));
    clients.put(client.address, client);
  }

  private void removeClient(Client client) {
    Platform.runLater(() -> {
      clientsObs.remove(client);
    });
    clients.remove(client.address);
    if(clients.isEmpty()) {
      clearClients();
      receiver.inGame = false;
    }
  }

  private void clearClients() {
    clients.clear();
    Platform.runLater(() -> clientsObs.clear());
    resetId();
  }

  private void resetId() {
    if(clients.isEmpty()) {
      if(method == Method.TCPUDP) {
        connections.forEach(c -> closeConnection(c));
      }
      id = 0;
      if(method == Method.TCPUDP) {
        openConnection();
      }
      logger.accept("Reset player IDs");
    }
  }

  public void openConnection() {
    Connection connection = new Connection(serverSocket, receiver, config);
    connections.add(connection);
    connection.start();
  }

  public void closeConnection(Connection connection) {
    connection.close();
    connections.remove(connection);
  }

  private Connection getConnection(InetAddress address) {
    for(Connection c : connections) {
      if(c.getSocket() != null) {
        if(c.getSocket().getInetAddress().equals(address)) {
          return c;
        }
      }
    }
    return null;
  }

  public Client getClient(InetAddress address) {
    return clients.get(address);
  }

  public LinkedList<Client> getClients() {
    LinkedList<Client> pls = new LinkedList<>();
    clients.entrySet().forEach(e -> pls.add(e.getValue()));
    return pls;
  }

  public LinkedList<Client> getPlayers() {
    LinkedList<Client> pls = new LinkedList<>();
    clients.entrySet().forEach(e -> {
      if(e.getValue().player) pls.add(e.getValue());
    });
    return pls;
  }

  public LinkedList<Client> getSpectators() {
    LinkedList<Client> pls = new LinkedList<>();
    clients.entrySet().forEach(e -> {
      if(!e.getValue().player) pls.add(e.getValue());
    });
    return pls;
  }

  public void broadcast(String message, boolean reliable, Client notClient) {
    clients.forEach((address, player) -> {
      if(player != notClient) {
        player.send(message, reliable);
      }
    });
  }

}
