package com.liveprogramming.kingslife.server.gui.logging;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Logger {

  private PrintWriter writer;
  private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_hh_mm_ss");

  public Logger() {
    try {
      File f = new File(formatter.format(LocalDateTime.now()) + ".log");
      if(!f.exists()) f.createNewFile();
      writer = new PrintWriter(f);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void log(String log) {
    try {
      writer.print(log);
      writer.flush();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void close() {
    writer.close();
  }

}
