package com.liveprogramming.kingslife.server.gui;

import com.liveprogramming.kingslife.server.ServerManager;
import com.liveprogramming.kingslife.server.gui.logging.Logger;
import com.liveprogramming.kingslife.server.model.Client;
import com.liveprogramming.kingslife.server.model.ClientProperty;
import com.liveprogramming.kingslife.server.gui.commands.*;
import com.liveprogramming.kingslife.server.update.UpdateManager;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

public class Controller {

  @FXML
  private TextArea taConsole;

  @FXML
  private TextField tfCli;

  @FXML
  private ListView liPlayers;

  @FXML
  private TableView tbProperties;

  @FXML
  private ObservableList<Client> clients;

  private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm:ss");
  private static Logger logger;
  private StringBuilder logBuilder = new StringBuilder();

  private Command[] commands = {new HelpCommand(), new StartCommand(), new StopCommand(), new ClearCommand(), new DeployCommand(), new ExitCommand()};
  private Command unknown = new UnkownCommand();

  private LinkedList<String> inputHistory = new LinkedList<>();
  private int currentPointer = -1;

  @FXML
  public void initialize() {
    try {
      logger = new Logger();
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    UpdateManager.getInstance().setLogger(this::log);
    UpdateManager.getInstance().start();

    ServerManager.getInstance().setLogger(this::log);
    ServerManager.getInstance().setPlayers(clients);
    ServerManager.getInstance().start(ServerManager.Method.JHUDP);

    tbProperties.getColumns().forEach(c -> {
      TableColumn tc = (TableColumn) c;
      tc.prefWidthProperty().bind(tbProperties.widthProperty().divide(tbProperties.getColumns().size()));
    });
    ((TableColumn)tbProperties.getColumns().get(0))
            .setCellValueFactory(cellValue -> new ReadOnlyStringWrapper(((ClientProperty)
                    ((TableColumn.CellDataFeatures) cellValue).getValue()).key.getReadOnlyProperty().getValue()));
    ((TableColumn)tbProperties.getColumns().get(1))
            .setCellValueFactory(cellValue -> ((ClientProperty) ((TableColumn.CellDataFeatures) cellValue).getValue()).value);
    tbProperties.setPlaceholder(new Label("Select a client"));

    Platform.runLater(() -> tfCli.requestFocus());
  }

  public void onChoosePlayer() {
    tbProperties.getItems().clear();
    Client client = (Client) liPlayers.getSelectionModel().getSelectedItem();
    if(client != null) {
      tbProperties.getItems().addAll(ClientProperty.fromPlayer(client));
    }
  }

  public void onKey(KeyEvent e) {
    int pointer = currentPointer;
    switch(e.getCode()) {
      case UP: {
        if(currentPointer == -1) {
          currentPointer = inputHistory.size() - 1;
        } else {
          if(currentPointer > 0) {
            currentPointer--;
          }
        }
        break;
      }
      case DOWN: {
        if(currentPointer != -1) {
          if(currentPointer < inputHistory.size() - 1) {
            currentPointer++;
          } else {
            currentPointer = -1;
            tfCli.setText("");
          }
        }
        break;
      }
      default: {
        currentPointer = -1;
        break;
      }
    }
    if(currentPointer != pointer && currentPointer != -1) tfCli.setText(inputHistory.get(currentPointer));
  }

  public void onCommand() {
    String text = tfCli.getText();
    String[] parts = text.split(" ");
    boolean executed = false;
    for(Command command : commands) {
      if(command.isCommand(parts[0])) {
        command.execute(this::log, parts);
        executed = true;
        break;
      }
    }
    if(!executed) {
      unknown.execute(this::log, parts);
    }
    tfCli.clear();

    if(!text.isEmpty()) inputHistory.add(text);
    if(inputHistory.size() >= 100) inputHistory.remove(0);
  }

  public synchronized void log(String log) {
    if(log.startsWith("/")) {
      switch(log) {
        case "/clear":
          taConsole.clear();
          break;
      }
    } else {
      logBuilder.append("[");
      logBuilder.append(formatter.format(LocalDateTime.now()));
      logBuilder.append("] ");
      logBuilder.append(log);
      logBuilder.append("\n");
      taConsole.appendText(logBuilder.toString());
      logger.log(logBuilder.toString());
      logBuilder.setLength(0);
    }
  }

}
