package com.liveprogramming.kingslife.server.gui.commands;

import com.liveprogramming.kingslife.server.ServerManager;
import com.liveprogramming.kingslife.server.update.UpdateManager;

import java.util.function.Consumer;

public class ExitCommand extends Command {

  public ExitCommand() {
    this.literal = "exit";
  }

  @Override
  public void execute(Consumer<String> output, String... parameters) {
    UpdateManager.getInstance().stop();
    ServerManager.getInstance().stop();
    System.exit(0);
  }
}
