package com.liveprogramming.kingslife.server.gui.commands;

import java.util.function.Consumer;

public class HelpCommand extends Command {

  public HelpCommand() {
    this.literal = "help";
  }

  @Override
  public void execute(Consumer<String> output, String... parameters) {
    output.accept("\n" +
            "Commands:\n" +
            "help - List of commands and their usage\n" +
            "start [method] - Start the server.\n" +
            "\t\t\tmethod: jhudp or tcpudp\n" +
            "stop - Stop the server\n" +
            "deploy [file] - Deploy the current game version to all clients\n" +
            "\t\t\tfile: Path to new game file\n" +
            "clear - Clear the console\n" +
            "exit - Exit the application");
  }

}
