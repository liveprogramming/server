package com.liveprogramming.kingslife.server.gui.commands;

import com.liveprogramming.kingslife.server.update.UpdateManager;

import java.io.File;
import java.util.function.Consumer;

public class DeployCommand extends Command {

  public DeployCommand() {
    this.literal = "deploy";
  }

  @Override
  public void execute(Consumer<String> output, String... parameters) {
    if(parameters.length > 1) {
      try {
        UpdateManager.getInstance().deploy(new File(parameters[1]));
      } catch (Exception ex) {
        output.accept("Illegal parameters");
      }
    } else {
      output.accept("Missing parameters");
    }
  }
}
