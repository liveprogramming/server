package com.liveprogramming.kingslife.server.gui.commands;

import java.util.function.Consumer;

public class UnkownCommand extends Command {

  @Override
  public void execute(Consumer<String> output, String... parameters) {
    output.accept("Unknown command '" + parameters[0] + "'. Use help for a list of commands.");
  }

}
