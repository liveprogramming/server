package com.liveprogramming.kingslife.server.gui.commands;

import com.liveprogramming.kingslife.server.ServerManager;

import java.util.function.Consumer;

public class StopCommand extends Command {

  public StopCommand() {
    this.literal = "stop";
  }

  @Override
  public void execute(Consumer<String> output, String... parameters) {
    ServerManager.getInstance().stop();
  }
}
