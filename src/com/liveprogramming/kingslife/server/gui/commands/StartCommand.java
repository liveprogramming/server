package com.liveprogramming.kingslife.server.gui.commands;

import com.liveprogramming.kingslife.server.ServerManager;

import java.util.function.Consumer;

public class StartCommand extends Command {

  public StartCommand() {
    this.literal = "start";
  }

  @Override
  public void execute(Consumer<String> output, String... parameters) {
    ServerManager.Method method = ServerManager.Method.JHUDP;
    if(parameters.length > 1) {
      if(parameters[1].equalsIgnoreCase("jhudp")) {
        method = ServerManager.Method.JHUDP;
      } else if(parameters[1].equalsIgnoreCase("tcpudp")) {
        method = ServerManager.Method.TCPUDP;
      } else if(parameters[1].equalsIgnoreCase("0")) {
        method = ServerManager.Method.JHUDP;
      } else if(parameters[1].equalsIgnoreCase("1")) {
        method = ServerManager.Method.TCPUDP;
      }
    }
    ServerManager.getInstance().start(method);
  }
}
