package com.liveprogramming.kingslife.server.gui.commands;

import java.util.function.Consumer;

public abstract class Command {

  protected String literal = "";

  public abstract void execute(Consumer<String> output, String... parameters);

  public boolean isCommand(String literal) {
    return this.literal.equalsIgnoreCase(literal);
  }

}
