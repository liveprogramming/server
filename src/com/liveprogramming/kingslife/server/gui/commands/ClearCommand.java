package com.liveprogramming.kingslife.server.gui.commands;

import java.util.function.Consumer;

public class ClearCommand extends Command {

  public ClearCommand() {
    this.literal = "clear";
  }

  @Override
  public void execute(Consumer<String> output, String... parameters) {
    output.accept("/clear");
  }
}
